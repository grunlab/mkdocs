[![pipeline status](https://gitlab.com/grunlab/mkdocs/badges/main/pipeline.svg)](https://gitlab.com/grunlab/mkdocs/-/commits/main)

# GrunLab MkDocs

MkDocs non-root container image.

Docs: https://docs.grunlab.net/images/mkdocs.md

Base image: [grunlab/base-image/debian:12][base-image]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this image:
- [grunlab/docs][docs]

[base-image]: <https://gitlab.com/grunlab/base-image>
[docs]: <https://gitlab.com/grunlab/docs>