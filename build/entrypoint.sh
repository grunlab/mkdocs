#!/bin/sh

MKDOCS_PROJECT_NAME=${MKDOCS_PROJECT_NAME:-my-project}

if [ ! -d "/var/lib/mkdocs/${MKDOCS_PROJECT_NAME}" ]
then
  mkdocs new /var/lib/mkdocs/${MKDOCS_PROJECT_NAME}
fi

cd /var/lib/mkdocs/${MKDOCS_PROJECT_NAME}
mkdocs serve -a 0.0.0.0:8000